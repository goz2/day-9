package com.afs.restapi.repository;


import com.afs.restapi.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query(
            value = "select * from employee e where e.gender = ?1",
            countQuery = "select count(*) from employee e where e.gender = ?1",
            nativeQuery = true
    )
    List<Employee> findByGender(String gender);

    @Query(
            value = "select * from employee e where e.company_id = ?1",
            countQuery = "select count(*) from employee e where e.company_id = ?1",
            nativeQuery = true
    )
    List<Employee> findByCompanyId(Long companyId);
}
